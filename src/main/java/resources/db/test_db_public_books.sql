create table books
(
    id     integer not null
        constraint books2_pk
            primary key,
    title  varchar not null,
    author varchar not null,
    year   integer not null,
    pages  integer not null
);

alter table books
    owner to postgres;

create unique index books2_id_uindex
    on books (id);

INSERT INTO public.books (id, title, author, year, pages) VALUES (1, 'book1', 'author1', 2020, 1);
INSERT INTO public.books (id, title, author, year, pages) VALUES (2, 'book2', 'author2', 2021, 2);
INSERT INTO public.books (id, title, author, year, pages) VALUES (3, 'book3', 'author3', 2022, 3);
INSERT INTO public.books (id, title, author, year, pages) VALUES (63, 'urfa', 'surfa', 2063, 63);