create table user_music
(
    id                integer not null
        constraint music_pk
            primary key,
    sarki             varchar not null,
    sarkici           varchar not null,
    user_id           integer
        constraint users_id_fk
            references users,
    like_user_list_id varchar
);

alter table user_music
    owner to postgres;

create unique index music_id_uindex
    on user_music (id);

INSERT INTO public.user_music (id, sarki, sarkici, user_id, like_user_list_id) VALUES (2, 'sarki2', 'sarkici2', 0, null);
INSERT INTO public.user_music (id, sarki, sarkici, user_id, like_user_list_id) VALUES (3, 'sarki3', 'sarkici3', 0, null);
INSERT INTO public.user_music (id, sarki, sarkici, user_id, like_user_list_id) VALUES (4, 'sarki4', 'sarkici4', 4, null);
INSERT INTO public.user_music (id, sarki, sarkici, user_id, like_user_list_id) VALUES (1, 'sarki', 'sarkici', 1, null);
INSERT INTO public.user_music (id, sarki, sarkici, user_id, like_user_list_id) VALUES (10, 'sarki10', 'sarkici10', 1, null);