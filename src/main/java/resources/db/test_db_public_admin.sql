create table admin
(
    id       integer not null
        constraint admin_pk
            primary key,
    name     varchar not null,
    surname  varchar not null,
    email    varchar not null,
    password varchar not null,
    role     varchar not null,
    premium  boolean not null
);

alter table admin
    owner to postgres;

create unique index admin_id_uindex
    on admin (id);

INSERT INTO public.admin (id, name, surname, email, password, role, premium) VALUES (1, 'MehmetA', 'Çakmak', 'mca', 'mca', 'admin', true);
INSERT INTO public.admin (id, name, surname, email, password, role, premium) VALUES (2, 'HalilA', 'Çakmak', 'hca', 'hca', 'admin', true);
INSERT INTO public.admin (id, name, surname, email, password, role, premium) VALUES (3, 'MükailA', 'Çakmak', 'muca', 'muca', 'admin', false);
INSERT INTO public.admin (id, name, surname, email, password, role, premium) VALUES (4, 'FatmaA', 'Çakmak', 'fca', 'fca', 'admin', true);
INSERT INTO public.admin (id, name, surname, email, password, role, premium) VALUES (5, 'SultanA', 'Çakmak', 'sca', 'sca', 'admin', true);