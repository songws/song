create table admin_music
(
    id              integer not null
        constraint music_admin_pk
            primary key,
    sarki           varchar not null,
    sarkici         varchar not null,
    admin_id        integer not null
        constraint "musicAdmin_admin___fk"
            references admin,
    like_admin_list varchar
);

alter table admin_music
    owner to postgres;

create unique index music_admin_id_uindex
    on admin_music (id);

INSERT INTO public.admin_music (id, sarki, sarkici, admin_id, like_admin_list) VALUES (2, 'halilSarki', 'halilSarkici', 2, null);
INSERT INTO public.admin_music (id, sarki, sarkici, admin_id, like_admin_list) VALUES (1, 'mehmetSarki', 'mehmetSarkici', 1, null);
INSERT INTO public.admin_music (id, sarki, sarkici, admin_id, like_admin_list) VALUES (4, 'halilSarki', 'halilSarkici', 2, null);
INSERT INTO public.admin_music (id, sarki, sarkici, admin_id, like_admin_list) VALUES (5, 'halilSarki', 'halilSarkici', 2, null);
INSERT INTO public.admin_music (id, sarki, sarkici, admin_id, like_admin_list) VALUES (6, 'halilSarki', 'halilSarkici', 2, null);
INSERT INTO public.admin_music (id, sarki, sarkici, admin_id, like_admin_list) VALUES (7, 'halilSarki', 'halilSarkici', 2, null);
INSERT INTO public.admin_music (id, sarki, sarkici, admin_id, like_admin_list) VALUES (3, 'mehmetSarki', 'mehmetSarkici', 1, null);
INSERT INTO public.admin_music (id, sarki, sarkici, admin_id, like_admin_list) VALUES (8, 'mehmetSarki', 'mehmetSarkici', 1, null);
INSERT INTO public.admin_music (id, sarki, sarkici, admin_id, like_admin_list) VALUES (9, 'mehmetSarki', 'mehmetSarkici', 1, null);
INSERT INTO public.admin_music (id, sarki, sarkici, admin_id, like_admin_list) VALUES (11, 'mükailSarki', 'MükailSarkici', 3, null);
INSERT INTO public.admin_music (id, sarki, sarkici, admin_id, like_admin_list) VALUES (12, 'mükailSarki', 'MükailSarkici', 3, null);
INSERT INTO public.admin_music (id, sarki, sarkici, admin_id, like_admin_list) VALUES (13, 'mükailSarki', 'MükailSarkici', 3, null);
INSERT INTO public.admin_music (id, sarki, sarkici, admin_id, like_admin_list) VALUES (14, 'mükailSarki', 'MükailSarkici', 3, null);
INSERT INTO public.admin_music (id, sarki, sarkici, admin_id, like_admin_list) VALUES (15, 'mükailSarki', 'mükailSarkici', 3, null);
INSERT INTO public.admin_music (id, sarki, sarkici, admin_id, like_admin_list) VALUES (20, 'fartmaSarki', 'fatmaSarkici', 4, null);
INSERT INTO public.admin_music (id, sarki, sarkici, admin_id, like_admin_list) VALUES (21, 'fartmaSarki', 'fatmaSarkici', 4, null);
INSERT INTO public.admin_music (id, sarki, sarkici, admin_id, like_admin_list) VALUES (22, 'fartmaSarki', 'fatmaSarkici', 4, null);
INSERT INTO public.admin_music (id, sarki, sarkici, admin_id, like_admin_list) VALUES (30, 'sutaSarki', 'sultanSarkici', 5, null);
INSERT INTO public.admin_music (id, sarki, sarkici, admin_id, like_admin_list) VALUES (33, 'sutaSarki', 'sultanSarkici', 5, null);
INSERT INTO public.admin_music (id, sarki, sarkici, admin_id, like_admin_list) VALUES (34, 'sutaSarki', 'sultanSarkici', 5, null);
INSERT INTO public.admin_music (id, sarki, sarkici, admin_id, like_admin_list) VALUES (10, 'mehmetSarki', 'mehmetSarkici', 1, null);