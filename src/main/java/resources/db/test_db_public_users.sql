create table users
(
    id       integer not null
        constraint users_pk
            primary key,
    name     varchar not null,
    surname  varchar not null,
    email    varchar not null,
    password varchar not null,
    role     varchar not null,
    premium  boolean default false
);

alter table users
    owner to postgres;

INSERT INTO public.users (id, name, surname, email, password, role, premium) VALUES (4, 'name', 'surname', 'email', 'pass', 'user', true);
INSERT INTO public.users (id, name, surname, email, password, role, premium) VALUES (0, 'Ahmet', 'Cacure', 'cacurehmo', '0', 'user', false);
INSERT INTO public.users (id, name, surname, email, password, role, premium) VALUES (3, 'Mükail', 'Çakmak', 'mucakmak', '3333', 'user', false);
INSERT INTO public.users (id, name, surname, email, password, role, premium) VALUES (1, 'Mehmet', 'Çakmak', 'mcakmak', '1111', 'user', true);
INSERT INTO public.users (id, name, surname, email, password, role, premium) VALUES (2, 'Halil', 'Çakmak', 'hcakmak', '2222', 'user', false);