package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import library.Musics;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;


public class MainUserController implements Initializable {

	int n;

    @FXML
    private TextField idField;

    @FXML
    private TextField sarkiField;

	@FXML
	private TextField sarkiciField;

	@FXML
	private TextField followUserId;

	@FXML
	private TableView<Musics> TableView;

	@FXML
	private TableColumn<Musics, Integer> idColumn;

	@FXML
	private TableColumn<Musics, String> sarkiColumn;

	@FXML
	private TableColumn<Musics, String> sarkiciColumn;

	@FXML
	private TableColumn<Musics, Integer> userIdColumn;

	@FXML
	private Button logoutBtn;

	@FXML
	private Button followBtn;

    @FXML
    private void insertButton() {
    	String query = "insert into user_music values('"+idField.getText()+"','"+sarkiField.getText()+"','"+sarkiciField.getText()+"','"+ SessionMapUser.getSessionMap("userData").getId()+"')";
    	executeQuery(query);
    	showMusics();
    }

	@FXML
	private void loginButton() throws IOException {
    	//Delete Session
    	SessionMapUser.deleteAllSession();
    	//Get Stage
		Stage stageMain = (Stage) logoutBtn.getScene().getWindow();
		//Close Stage
		stageMain.close();

		Stage stage2 = new Stage();
		Parent parent = FXMLLoader.load(getClass().getResource("/view/LoginUser.fxml"));
		Scene scene = new Scene(parent, 300, 150);
		stage2.setScene(scene);
		stage2.setTitle("Login User Page");
		stage2.show();
	}
    
    
    @FXML 
    private void updateButton() {
		String query = "UPDATE user_music SET sarki='"+sarkiField.getText()+"',sarkici='"+sarkiciField.getText()+"',user_id='"+ SessionMapUser.getSessionMap("userData").getId()+"' WHERE ID='"+idField.getText()+"'";
		executeQuery(query);
		showMusics();
    }
    
    @FXML
    private void deleteButton() {
    	String query = "DELETE FROM user_music WHERE ID="+idField.getText()+"";
    	executeQuery(query);
		showMusics();
    }
    
    public void executeQuery(String query) {
    	Connection conn = getConnection();
    	Statement st;
    	try {
			st = conn.createStatement();
			st.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
		followUserId.setText(SessionMapUser.getSessionMap("userData").getId()+"");
		System.out.println(SessionMapUser.getSessionMap("userData").getName());
		System.out.println(SessionMapUser.getSessionMap("userData").getName());
		showMusics();
    }

	@FXML
	private void followButton() throws IOException {
		n++;
		if(n%2==1) {
			if (!followUserId.getText().equals("")) {
				System.out.println(followUserId.getText());
				followBtn.setText("Un Follow");
				showMusics();
			}
		}else{
			followUserId.setText(SessionMapAdmin.getSessionMap("userData").getId()+"");
			followBtn.setText("Follow");
			showMusics();
		}
	}
    
    public Connection getConnection() {
    	Connection conn;
    	try {
    		conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/test_db","postgres","1111");
    		return conn;
    	}
    	catch (Exception e){
    		e.printStackTrace();
    		return null;
    	}
    }

	public ObservableList<Musics> getMusicsList(){
		ObservableList<Musics> musicsList = FXCollections.observableArrayList();
		Connection connection = getConnection();
		String query = "SELECT * FROM user_music";
		Statement st;
		ResultSet rs;

		try {
			st = connection.createStatement();
			rs = st.executeQuery(query);
			Musics musics;


			String query2 = "SELECT * FROM users WHERE id='"+followUserId.getText()+"'";
			Statement st2;
			ResultSet rs2;

			st2 = connection.createStatement();
			rs2 = st2.executeQuery(query2);
			rs2.next();

			while(rs.next()) {
				musics = new Musics(rs.getInt("Id"),rs.getString("Sarki"),rs.getString("Sarkici"),rs.getInt("User_id"));
				if (SessionMapUser.getSessionMap("userData").getId()==rs.getInt("User_id") || (rs2.getInt("Id")==rs.getInt("User_id") && rs2.getBoolean("Premium"))){
					musicsList.add(musics);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return musicsList;
	}

	// I had to change ArrayList to ObservableList I didn't find another option to do this but this works :)
	public void showMusics() {
		ObservableList<Musics> list = getMusicsList();

		idColumn.setCellValueFactory(new PropertyValueFactory<Musics,Integer>("id"));
		sarkiColumn.setCellValueFactory(new PropertyValueFactory<Musics,String>("sarki"));
		sarkiciColumn.setCellValueFactory(new PropertyValueFactory<Musics,String>("sarkici"));
		userIdColumn.setCellValueFactory(new PropertyValueFactory<Musics,Integer>("userId"));

		TableView.setItems(list);
	}

}
