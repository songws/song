package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import library.Users;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;


public class RegisterUserController implements Initializable {

    @FXML
    private TextField idField;

	@FXML
    private TextField nameField;

    @FXML
    private TextField surnameField;

    @FXML
    private TextField emailField;

    @FXML
    private TextField passwordField;

	@FXML
	private CheckBox premiumField;

	@FXML
	private Button loginBtn;

    @FXML
    private void insertButton() {
    	String query = "insert into users values('"+idField.getText()+"','"+nameField.getText()+"','"+surnameField.getText()+"','"+emailField.getText()+"','"+passwordField.getText()+"','user','"+premiumField.isSelected()+"')";
    	executeQuery(query);
    }

	@FXML
	private void loginButton() throws IOException {
    	//Get Stage
		Stage stageMain = (Stage) loginBtn.getScene().getWindow();
		//Close Stage
		stageMain.close();

		Stage stage2 = new Stage();
		Parent parent = FXMLLoader.load(getClass().getResource("/view/LoginUser.fxml"));
		Scene scene = new Scene(parent, 300, 150);
		stage2.setScene(scene);
		stage2.setTitle("Login User Page");
		stage2.show();
	}
    
    
    @FXML 
    private void updateButton() {
    String query = "UPDATE users SET name='"+nameField.getText()+"',surname='"+surnameField.getText()+"',email='"+emailField.getText()+"',password='"+passwordField.getText()+"' WHERE ID='"+idField.getText()+"'";
    executeQuery(query);
    }
    
    @FXML
    private void deleteButton() {
    	String query = "DELETE FROM users WHERE ID="+idField.getText()+"";
    	executeQuery(query);
    }
    
    public void executeQuery(String query) {
    	Connection conn = getConnection();
    	Statement st;
    	try {
			st = conn.createStatement();
			st.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
    
    public Connection getConnection() {
    	Connection conn;
    	try {
    		conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/test_db","postgres","1111");
    		return conn;
    	}
    	catch (Exception e){
    		e.printStackTrace();
    		return null;
    	}
    }
    
    public ObservableList<Users> getUsersList(){
    	ObservableList<Users> booksList = FXCollections.observableArrayList();
    	Connection connection = getConnection();
    	String query = "SELECT * FROM users";
    	Statement st;
    	ResultSet rs;
    	
    	try {
			st = connection.createStatement();
			rs = st.executeQuery(query);
			Users users;
			while(rs.next()) {
				users = new Users(rs.getInt("Id"),rs.getString("Name"),rs.getString("Surname"),rs.getString("Email"),rs.getString("Password"),rs.getString("role"),rs.getBoolean("premium"));
				booksList.add(users);
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return booksList;
    }

}
