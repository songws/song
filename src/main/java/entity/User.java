package entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user")
@Getter
@Setter
public class User {

    @Id
    @Column(name = "id")
    private Integer id;
    @Column(name = "username")
    private String username;
    @Column(name = "email")
    private String email;
    @Column(name = "gsm")
    private String gsm;
    @Column(name = "password")
    private String password;
    @Column(name = "subscription_type")
    private Integer subscriptionType;
    @Column(name = "user_type")
    private Integer userType;

}
