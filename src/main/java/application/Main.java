package application;

import entity.User;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import repository.Db;

import javax.persistence.EntityManager;

public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource("/view/Main.fxml"));
        Scene scene = new Scene(parent, 400, 100);
        stage.setScene(scene);
        stage.setTitle("Select Page");
        stage.show();

        /*
        Parent parent = FXMLLoader.load(getClass().getResource("/view/MainPane.fxml"));
        Scene scene = new Scene(parent, 700, 600);
        stage.setScene(scene);
        stage.setTitle("Library");
        stage.show();
        * */
    }

    public static void main(String[] args) {

        EntityManager entityMgr = Db.getEntityManager();
        entityMgr.getTransaction().begin();

        User user = new User();
        user.setId(101);

        user.setUsername("mehmet.cakmak");
        user.setEmail("mehmet@gmail.com");
        user.setGsm("0544444");
        user.setPassword("4242424");
        user.setSubscriptionType(1);
        user.setUserType(1);
        entityMgr.persist(user);

        entityMgr.getTransaction().commit();

        entityMgr.clear();
        System.out.println("Record Successfully Inserted In The Database");

        launch(args);
    }
}
